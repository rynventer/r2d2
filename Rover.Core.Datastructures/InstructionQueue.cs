﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstructionQueue.cs" company="">
//   
// </copyright>
// <summary>
//   The instruction queue.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover.Core.Datastructures
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// This class is a specialised queue that is specifically created to store instructions
    /// that the spacewagons can follow. The controller takes an instance of this class in
    /// order to control the spacewagon
    /// </summary>
    public class InstructionQueue
    {
        #region Constants

        /// <summary>
        /// Character that represents turning left
        /// </summary>
        private const char leftCharacter = 'L';

        /// <summary>
        /// Character that represents moving forward
        /// </summary>
        private const char moveCharacter = 'M';

        /// <summary>
        /// Character that represents turning right
        /// </summary>
        private const char rightCharacter = 'R';

        #endregion

        #region Fields

        /// <summary>
        /// The encapsulated queue object that is used to enqueue and dequeue instructions
        /// </summary>
        private readonly Queue<Instruction> instructionQueue;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="InstructionQueue"/> class.
        /// This constructor starts with an empty instruction queue
        /// </summary>
        public InstructionQueue()
        {
            // Initialise members
            this.instructionQueue = new Queue<Instruction>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstructionQueue"/> class.
        /// This constructor takes an initial string of instructions in order to populate the encapsulated queue
        /// </summary>
        /// <param name="instructions">
        /// A string of characters that represent either turn left, turn right, move forward.
        /// Please note that invalid instructions will not be taken into account.
        /// </param>
        public InstructionQueue(string instructions)
        {
            // Initialise members
            this.instructionQueue = new Queue<Instruction>();

            // Populate the queue with the instruction string
            foreach (var c in instructions)
            {
                switch (c)
                {
                    case moveCharacter:
                        {
                            this.EnqueueInstruction(Instruction.MoveForward);
                            break;
                        }

                    case leftCharacter:
                        {
                            this.EnqueueInstruction(Instruction.TurnLeft);
                            break;
                        }

                    case rightCharacter:
                        {
                            this.EnqueueInstruction(Instruction.TurnRight);
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }
            }
        }

        #endregion

        #region Enums

        /// <summary>
        /// The instruction.
        /// </summary>
        public enum Instruction
        {
            /// <summary>
            /// The turn left enumerator
            /// </summary>
            TurnLeft,

            /// <summary>
            /// The turn right enumerator
            /// </summary>
            TurnRight,

            /// <summary>
            /// The move forward enumerator
            /// </summary>
            MoveForward,

            /// <summary>
            /// Represents a no operation instruction
            /// </summary>
            Nothing
        }

        #endregion

        #region Public methods and operators

        /// <summary>
        /// The dequeue instruction method will return an instruction from the front of the queue.
        /// </summary>
        /// <returns>
        /// The return value is the instruction at the front of the queue. If there are no instructions, the Nothing enumerator will be returned <see cref="Instruction"/>.
        /// </returns>
        public Instruction DequeueInstruction()
        {
            // First make sure there are existing instructions
            if (this.instructionQueue.Count > 0)
            {
                return this.instructionQueue.Dequeue();
            }

            // Else return a blank instruction
            return Instruction.Nothing;
        }

        /// <summary>
        /// The enqueue instruction method will add a new instruction ot the back of the queue.
        /// </summary>
        /// <param name="instruction">
        /// The instruction to be enqueued. Will not enqueue no operation (Nothing) instructions
        /// </param>
        public void EnqueueInstruction(Instruction instruction)
        {
            // Enqueue new instruction, except if it's nothing
            if (instruction != Instruction.Nothing)
            {
                this.instructionQueue.Enqueue(instruction);
            }
        }

        #endregion
    }
}