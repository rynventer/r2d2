﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Curiosity.cs" company="">
//   
// </copyright>
// <summary>
//   The curiosity.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover.Core.Datastructures
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The curiosity class that is a type of SpaceWagon
    /// </summary>
    internal class Curiosity : SpaceWagon
    {
        #region Constants

        /// <summary>
        /// The string name of the curiosity spacewagon
        /// </summary>
        private const string curiosityName = "Curiosity";

        /// <summary>
        /// The speed of the curiosity spacewagon per movement
        /// </summary>
        private const uint curiositySpeed = 2;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Curiosity"/> class.
        /// </summary>
        /// <param name="zone">
        /// The zone the curiosity spacewagon will reside in
        /// </param>
        /// <param name="startX">
        /// The start x position of the curiosity spacewagon.
        /// </param>
        /// <param name="startY">
        /// The start y position of the curiosity spacewagon.
        /// </param>
        /// <param name="direction">
        /// The initial facing direction of the curiosity spacewagon.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// The curiosity spacewagon has not been implemented yet.
        /// </exception>
        public Curiosity(Zone zone, uint startX, uint startY, char direction)
            : base(zone, startX, startY, direction)
        {
            throw new NotImplementedException("Curiosity hasn't been implemented.");
        }

        #endregion

        #region Other Methods

        /// <summary>
        /// Returns the name of the Curiosity.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// Returns the name of the Curiosity.
        /// </returns>
        protected override string GetName()
        {
            return curiosityName;
        }

        /// <summary>
        /// Returns the speed of the Curiosity
        /// </summary>
        /// <returns>
        /// The <see cref="uint"/>.
        /// Returns the speed of the Curiosity
        /// </returns>
        protected override uint GetSpeed()
        {
            return curiositySpeed;
        }

        #endregion
    }
}