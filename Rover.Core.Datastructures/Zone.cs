﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Zone.cs" company="">
//   
// </copyright>
// <summary>
//   The zone.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover.Core.Datastructures
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The zone class represents a zone with boundaries that spacewagons can move around in.
    /// </summary>
    public class Zone
    {
        #region Fields

        /// <summary>
        /// The zone height.
        /// </summary>
        private uint zoneHeight;

        /// <summary>
        /// The zone width.
        /// </summary>
        private uint zoneWidth;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Zone"/> class.
        /// </summary>
        /// <param name="width">
        /// The width of the zone.
        /// </param>
        /// <param name="height">
        /// The height of the zone.
        /// </param>
        public Zone(uint width, uint height)
        {
            this.ZoneHeight = height;
            this.ZoneWidth = width;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets and sets the zone height.
        /// </summary>
        /// <exception cref="ArgumentException">
        /// A zone height cannot be zero
        /// </exception>
        public uint ZoneHeight
        {
            get
            {
                return this.zoneHeight;
            }

            private set
            {
                // Make sure height is more than zero
                if (value > 0)
                {
                    this.zoneHeight = value;
                }
                else
                {
                    throw new ArgumentException("Zone height can not be zero.");
                }
            }
        }

        /// <summary>
        /// Gets and sets the zone width.
        /// </summary>
        /// <exception cref="ArgumentException">
        /// A zone width cannot be zero
        /// </exception>
        public uint ZoneWidth
        {
            get
            {
                return this.zoneWidth;
            }

            private set
            {
                // Don't wanna set this from outside
                // Make sure width is more than zero
                if (value > 0)
                {
                    this.zoneWidth = value;
                }
                else
                {
                    throw new ArgumentException("Zone width can not be zero.");
                }
            }
        }

        #endregion
    }
}