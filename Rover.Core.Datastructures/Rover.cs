﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Rover.cs" company="">
//   
// </copyright>
// <summary>
//   The rover.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover.Core.Datastructures
{
    /// <summary>
    /// The rover child implementation of the abstract SpaceWagon class
    /// </summary>
    public sealed class Rover : SpaceWagon
    {
        #region Constants

        /// <summary>
        /// The name of the Rover
        /// </summary>
        private const string roverName = "Rover";

        /// <summary>
        /// The speed of the Rover
        /// </summary>
        private const uint roverSpeed = 1;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Rover"/> class.
        /// </summary>
        /// <param name="zone">
        /// The zone in which the rover will reside
        /// </param>
        /// <param name="startX">
        /// The x value of the rover's starting location
        /// </param>
        /// <param name="startY">
        /// The y value of the rover's starting location
        /// </param>
        /// <param name="direction">
        /// The initial direction in which the rover is facing
        /// </param>
        public Rover(Zone zone, uint startX, uint startY, char direction)
            : base(zone, startX, startY, direction)
        {
        }

        #endregion

        #region Other Methods

        /// <summary>
        /// Implementation of parent class abstract GetName function.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// Returns the name of a Rover
        /// </returns>
        protected override string GetName()
        {
            return roverName;
        }

        /// <summary>
        /// Implementation of the parent class abstract GetSpeed function
        /// </summary>
        /// <returns>
        /// The <see cref="uint"/>.
        /// Returns the speed of a Rover
        /// </returns>
        protected override uint GetSpeed()
        {
            return roverSpeed;
        }

        #endregion
    }
}