﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpaceWagon.cs" company="">
//   
// </copyright>
// <summary>
//   The space wagon.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover.Core.Datastructures
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The space class is an abstract class that contains basic functionality of all spacewagons (e.g. Rover, Curiosity)
    /// Children of this class differ in speed and name
    /// A spacewagon will automatically refuse to exit the zone it is in for safety reasons
    /// </summary>
    public abstract class SpaceWagon
    {
        #region Fields

        /// <summary>
        /// The name of the SpaceWagon.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// The speed of the SpaceWagon (blocks moved per move instruction).
        /// </summary>
        private readonly uint speed;

        /// <summary>
        /// The x coordinate of the SpaceWagon.
        /// </summary>
        private uint x;

        /// <summary>
        /// The y coordinate of the SpaceWagon.
        /// </summary>
        private uint y;

        /// <summary>
        /// A reference to the zone that the SpaceWagon operates in.
        /// </summary>
        private readonly Zone Zone;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SpaceWagon"/> class.
        /// </summary>
        /// <param name="zone">
        /// A reference to the zone that the SpaceWagon operates in..
        /// </param>
        /// <param name="startingX">
        /// The starting x coordinate of the SpaceWagon.
        /// </param>
        /// <param name="startingY">
        /// The starting y coordinate of the SpaceWagon.
        /// </param>
        /// <param name="direction">
        /// The initial direction of the SpaceWagon.
        /// Note: Can only be characters 'N', 'W', 'S' or 'E'. ArgumentException will be throw otherwise
        /// </param>
        public SpaceWagon(Zone zone, uint startingX, uint startingY, char direction)
        {
            // Initialise members from parameters
            this.Zone = zone;
            this.X = startingX;
            this.Y = startingY;
            this.Direction = CharToFacingDirection(direction);

            // Get speed and name depending on the child class's implementation
            this.speed = this.GetSpeed();
            this.Name = this.GetName();
        }

        #endregion

        #region Enums

        /// <summary>
        /// Enumerator representing the current direction that the SpaceWagon faces
        /// </summary>
        public enum FacingDirection
        {
            /// <summary>
            /// The direction north.
            /// </summary>
            North,

            /// <summary>
            /// The direction west.
            /// </summary>
            West,

            /// <summary>
            /// The direction south.
            /// </summary>
            South,

            /// <summary>
            /// The direction east.
            /// </summary>
            East
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the current facing direction of the SpaceWagon. Cannot set this from outside the class.
        /// </summary>
        public FacingDirection Direction { get; private set; }

        /// <summary>
        /// Gets and sets the x coordinate of the SpaceWagon.
        /// Can not be less than 1, cannot be more than the zone it resides in's width
        /// </summary>
        public uint X
        {
            get
            {
                return this.x;
            }

            private set
            {
                // Don't want to set this from outside of this class

                // Make sure we don't take the SpaceWagon out of our zone
                // First check the minimum (can't go to a position smaller than one)
                if (value < 1)
                {
                    this.x = 1;
                }

                // Now check the maximum
                else if (value > this.Zone.ZoneWidth)
                {
                    this.x = this.Zone.ZoneWidth;
                }
                else
                {
                    // Only other possible case is that it falls in an accepted spot
                    this.x = value;
                }
            }
        }

        /// <summary>
        /// Gets and sets the y coordinate of the SpaceWagon.
        /// Can not be less than 1, cannot be more than the zone it resides in's height
        /// </summary>
        public uint Y
        {
            get
            {
                return this.y;
            }

            private set
            {
                // Don't want to set this from outside of this class

                // Make sure we don't take the SpaceWagon out of our zone
                // First check the minimum (can't go to a position smaller than one)
                if (value < 1)
                {
                    this.y = 1;
                }

                // Now check the maximum
                else if (value > this.Zone.ZoneHeight)
                {
                    this.y = this.Zone.ZoneWidth;
                }
                else
                {
                    // Only other possible case is that it falls in an accepted spot
                    this.y = value;
                }
            }
        }

        #endregion

        #region Public methods and operators

        /// <summary>
        /// This method moves the SpaceWagon forward according to its inherent speed
        /// </summary>
        public void MoveForward()
        {
            switch (this.Direction)
            {
                case FacingDirection.North:
                    {
                        this.Y += this.speed;
                        break;
                    }

                case FacingDirection.West:
                    {
                        this.X -= this.speed;
                        break;
                    }

                case FacingDirection.South:
                    {
                        this.Y -= this.speed;
                        break;
                    }

                case FacingDirection.East:
                    {
                        this.X += this.speed;
                        break;
                    }
            }
        }

        /// <summary>
        /// This method returns a string representing the current position and direction of the SpaceWagon
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// The current position and direction of the SpaceWagon in format "X Y Direction"
        /// </returns>
        public string StatusToString()
        {
            return string.Format("{0} {1} {2}", this.X, this.Y, this.Direction);
        }

        /// <summary>
        /// Turn the SpaceWagon to the left
        /// </summary>
        public void TurnLeft()
        {
            switch (this.Direction)
            {
                case FacingDirection.North:
                    {
                        this.Direction = FacingDirection.West;
                        break;
                    }

                case FacingDirection.West:
                    {
                        this.Direction = FacingDirection.South;
                        break;
                    }

                case FacingDirection.South:
                    {
                        this.Direction = FacingDirection.East;
                        break;
                    }

                case FacingDirection.East:
                    {
                        this.Direction = FacingDirection.North;
                        break;
                    }
            }
        }

        /// <summary>
        /// Turn the SpaceWagon to the right
        /// </summary>
        public void TurnRight()
        {
            switch (this.Direction)
            {
                case FacingDirection.North:
                    {
                        this.Direction = FacingDirection.East;
                        break;
                    }

                case FacingDirection.East:
                    {
                        this.Direction = FacingDirection.South;
                        break;
                    }

                case FacingDirection.South:
                    {
                        this.Direction = FacingDirection.West;
                        break;
                    }

                case FacingDirection.West:
                    {
                        this.Direction = FacingDirection.North;
                        break;
                    }
            }
        }

        #endregion

        #region Other Methods

        /// <summary>
        /// Abstract function that returns the name of a child class. All children must have a name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// The name of the SpaceWagon
        /// </returns>
        protected abstract string GetName();

        /// <summary>
        /// Abstract function that returns the speed of a child class. All children must have a speed.
        /// </summary>
        /// <returns>
        /// The <see cref="uint"/>.
        /// The speed of the SpaceWagon
        /// </returns>
        protected abstract uint GetSpeed();

        /// <summary>
        /// Helper function that converts a character representing a direction to an enumerator
        /// </summary>
        /// <param name="d">
        /// The character that represents the direction - can only be 'N', 'E', 'W' or 'S'
        /// </param>
        /// <returns>
        /// The <see cref="FacingDirection"/>.
        /// The direction represented by an enumerator
        /// </returns>
        /// <exception cref="ArgumentException">
        /// If the character is not within the accepted subset of values, an argument exception will be thrown
        /// </exception>
        private static FacingDirection CharToFacingDirection(char d)
        {
            switch (d)
            {
                case 'N':
                    {
                        return FacingDirection.North;
                    }

                case 'W':
                    {
                        return FacingDirection.West;
                    }

                case 'E':
                    {
                        return FacingDirection.East;
                    }

                case 'S':
                    {
                        return FacingDirection.South;
                    }

                default:
                    {
                        throw new ArgumentException(string.Format("{0} not a valid direction", d));
                    }
            }
        }

        #endregion
    }
}