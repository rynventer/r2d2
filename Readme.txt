Rover Readme 
--------------

[Introduction]
Rover is a piece of software that simulates the movement of the Rover in a safe zone on the Planet Mars.
The application takes a text file as input. This text file contains information about the safe zone,
the initial positioning info of the Rover and then outputs either the ending position or errors that
might have occurred.

[Using Rover]
The Rover application can be started by supplying the location of the input file as the one and only argument.

usage: app_name.exe <"filename">
    Example: Rover.exe "c:\inputFile.txt"

[Input file]
The input file parsed by the Rover application has a strict format to adhere to. The input file consists of
exactly 3 lines.

The first line contains two digits seperated by a single space. The two digits represent the size of the safezone's
height and width respectively. Neither of the digits are allowed to be smaller than 1.

The second line contains two digits and one letter, each seperated by a single space. The first digit represents the
starting X coordinate of the Rover within the safe zone. The second digit represents the starting Y coordinate of the
Rover within the safe zone. The third letter represents the initial distance the Rover will be facing. The digits may
not result in the Rover starting without the safe zone. The letter must be a capital letter that falls within the 
subset {N, S, W, E}. These letters represent directions North, South, West and East respectively.

The third line represents a string of instructions given to the Rover. There is no limit to how many instructions are
given, but letters are limited to the subset {L, R, M}. These represent 'Turn Left', 'Turn Right' and 'Move Forward'
respectively.

The following is an example of a valid input file. The rover starts at (1,1) facing East. The safezone has width 4
and height 8. The Rover will be commanded to turn left, move forward, turn right and move forward again:

4 8
1 1 E
LMRM