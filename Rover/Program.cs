﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover
{
    #region

    using System;

    using Rover.Core.Datastructures;
    using Rover.Core.Process;

    #endregion

    /// <summary>
    /// The main program class.
    /// </summary>
    internal class Program
    {
        #region Constants

        /// <summary>
        /// The total arguments expected when running this program.
        /// </summary>
        private const int totalArguments = 1;

        #endregion

        #region Static Fields

        /// <summary>
        /// The usage information that is shown to the user
        /// </summary>
        private static readonly string[] usageInformation = { "usage: app_name.exe <\"filename\">", "    Example: Rover.exe \"c:\\inputFile.txt\""};

        /// <summary>
        /// The filename of the input file.
        /// </summary>
        private static string filename = string.Empty;

        #endregion

        #region Other Methods

        /// <summary>
        /// Creates a zone, spacewagon, controller and an instruction queue from the input file.
        /// Passes the necessary paramaters to the created objects and starts the driving process.
        /// Also prints the resulting location and direction of the SpaceWagon
        /// </summary>
        /// <param name="contents">
        /// Struct representing the contents of the input file
        /// </param>
        private static void FireItUp(InputFileContents contents)
        {
            // Create a zone
            Zone zone = new Zone(contents.ZoneWidth, contents.ZoneLength);

            // Create a spacewagon, which will be a rover in this case and assign it a zone
            SpaceWagon rover = new Rover(zone, contents.StartingX, contents.StartingY, contents.StartingDirection);

            // Create a controller and pass the rover and zone
            Controller controller = new Controller(zone, rover);

            // Create an instruction queue from the input file's instructions
            InstructionQueue instructionQueue = new InstructionQueue(new string(contents.Commands));

            // Start the controller
            controller.Go(instructionQueue);

            // Print the resulting position
            Console.Out.WriteLine("Resulting position of {0}: {1}", rover.Name, controller.GetSpaceWagonStatus());
        }

        /// <summary>
        /// The main function controls the logic from a high level
        /// This function is the entry point of the application
        /// This includes processing input arguments, processing the input file, starting the controller and
        /// showing necessary output.
        /// </summary>
        /// <param name="args">
        /// The args received from the command line
        /// </param>
        private static void Main(string[] args)
        {
            string filename;

            // Process input arguments
            // If arguments are incorrect...
            if (!processArguments(args, out filename))
            {
                // Print usage information to user
                printUsageInformation();

                // Wait for output
                Console.In.Read();

                // Exit with an error exit code
                Environment.Exit(-1);
            }
            else
            {
                // Create file contents structure
                InputFileContents contents;

                // Attempt to parse input file
                var result = InputFileManager.ParseAndValidateInputFile(filename, out contents);

                // If the result was not a success
                if (result != InputFileManager.InputFileParseResult.Success)
                {
                    // Let the user know
                    Console.Out.WriteLine("Error when parsing input file: {0}", result);

                    // Wait for output
                    Console.In.Read();

                    // Exit with an error code
                    Environment.Exit(-1);
                }
                else
                {
                    // Good to go, fire it up
                    FireItUp(contents);

                    // Wait for output
                    Console.In.Read();

                    // Exit gracefully
                    Environment.Exit(0);
                }
            }
        }

        /// <summary>
        /// Prints the usage information from the commandline
        /// </summary>
        private static void printUsageInformation()
        {
            foreach (var ui in usageInformation)
            {
                Console.Out.WriteLine(ui);
            }
        }

        /// <summary>
        /// This function validates and processes the arguments received from the command line.
        /// </summary>
        /// <param name="args">
        /// The args received from the command line
        /// </param>
        /// <param name="filename">
        /// An output variable that is populated with the filename if the arguments were considered valid
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// A boolean which is set to true if the arguments were successfully processed, and set to false if not.
        /// </returns>
        private static bool processArguments(string[] args, out string filename)
        {
            // Initial return value of output parameter
            filename = string.Empty;

            // Make sure the reference to the argument array isn't set to null
            if (args == null)
            {
                return false;
            }

            // Make sure we have the right amount of arguments
            if (args.Length != totalArguments)
            {
                return false;
            }

            // Extract the filename
            filename = args[0];

            return true;
        }

        #endregion
    }
}