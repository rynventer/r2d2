﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputFileManager.cs" company="">
//   
// </copyright>
// <summary>
//   The input file manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover
{
    #region

    using System;
    using System.IO;
    using System.Text.RegularExpressions;

    #endregion

    /// <summary>
    /// The input file manager reads and validates a text input file that represents the zone, starting position, starting direction and instructions.
    /// </summary>
    internal class InputFileManager
    {
        #region Constants

        /// <summary>
        /// The expected number of lines in the input file.
        /// </summary>
        private const int inputFilesLines = 3;

        #endregion

        #region Enums

        /// <summary>
        /// The input file parse result.
        /// </summary>
        public enum InputFileParseResult
        {
            /// <summary>
            /// Input file was parsed successfully.
            /// </summary>
            Success,

            /// <summary>
            /// Error reading file due to accessibility issues. This may be that the file is in use or does not exist.
            /// </summary>
            ErrorReadingFile,

            /// <summary>
            /// Input file does not contain the right amount of lines.
            /// </summary>
            IncorrectNumberOfLines,

            /// <summary>
            /// There is a format error in the first line of the file.
            /// </summary>
            FirstLineFormatError,

            /// <summary>
            /// There is a format error in the second line of the file.
            /// </summary>
            SecondLineFormatError,

            /// <summary>
            /// There is a format error in the third line of the file.
            /// </summary>
            ThirdLineFormatError,

            /// <summary>
            /// The starting position is out of the zone boundaries
            /// </summary>
            StartingPositionOutOfBoundaries,

            /// <summary>
            /// The zone too small - either the height or width of the zone is smaller than one
            /// </summary>
            ZoneTooSmall
        }

        #endregion

        #region Public methods and operators

        /// <summary>
        /// This method reads, validates and parses an input fule
        /// </summary>
        /// <param name="filename">
        /// The path to the file
        /// </param>
        /// <param name="inputFileContents">
        /// A return variable that contains the contents of the input file within a structure object
        /// </param>
        /// <returns>
        /// Returns the result of the method's read, parse and validation.
        /// </returns>
        public static InputFileParseResult ParseAndValidateInputFile(
            string filename,
            out InputFileContents inputFileContents)
        {
            // Initialise return structure
            inputFileContents = new InputFileContents();

            // Initialise file content array
            string[] fileContents;

            // Get file contents
            try
            {
                fileContents = File.ReadAllLines(filename);
            }
            catch (Exception)
            {
                return InputFileParseResult.ErrorReadingFile;
            }

            // Make sure there's the required amount of lines
            if (fileContents.Length != inputFilesLines)
            {
                return InputFileParseResult.IncorrectNumberOfLines;
            }

            // Evaluate first line. Two numbers seperated by a space is expected
            Regex regex = new Regex(@"^\d\s\d$");
            Match match = regex.Match(fileContents[0]);

            // Return if not successful
            if (!match.Success)
            {
                return InputFileParseResult.FirstLineFormatError;
            }

            // Evaluate second line. Two numbers and one letter of subset (N S W or E) is expected - all seperated by a space.
            regex = new Regex(@"^\d\s\d\s[NSWEnswe]$");
            match = regex.Match(fileContents[1]);

            // Return if not successful
            if (!match.Success)
            {
                return InputFileParseResult.SecondLineFormatError;
            }

            // Evaluate third line. A string of letters in subset MRL is expected.
            regex = new Regex(@"^[MRL]+$");
            match = regex.Match(fileContents[2]);

            // Return if not successful
            if (!match.Success)
            {
                return InputFileParseResult.ThirdLineFormatError;
            }

            // Now the lines need to be parsed in order to populate the return structure
            inputFileContents.ZoneWidth = Convert.ToUInt32(fileContents[0].Split(' ')[0]);
            inputFileContents.ZoneLength = Convert.ToUInt32(fileContents[0].Split(' ')[1]);
            inputFileContents.StartingX = Convert.ToUInt32(fileContents[1].Split(' ')[0]);
            inputFileContents.StartingY = Convert.ToUInt32(fileContents[1].Split(' ')[1]);
            inputFileContents.StartingDirection = fileContents[1].Split(' ')[2][0];
            inputFileContents.Commands = fileContents[2].ToCharArray();

            // Do some size validations
            // Zone sizes need to be an integer larger than 0
            if (inputFileContents.ZoneWidth < 1 || inputFileContents.ZoneLength < 1)
            {
                return InputFileParseResult.ZoneTooSmall;
            }

            // Starting spot X needs to fall within bounds
            if (inputFileContents.StartingX < 1 || inputFileContents.StartingX > inputFileContents.ZoneWidth)
            {
                return InputFileParseResult.StartingPositionOutOfBoundaries;
            }

            // Starting spot Y needs to fall within bounds
            if (inputFileContents.StartingY < 1 || inputFileContents.StartingY > inputFileContents.ZoneLength)
            {
                return InputFileParseResult.StartingPositionOutOfBoundaries;
            }

            // If we reached this point, validation and parsing has passed.
            return InputFileParseResult.Success;
        }

        #endregion
    }

    /// <summary>
    /// This is a structure that represents the input file contents as an object
    /// </summary>
    internal struct InputFileContents
    {
        /// <summary>
        /// The zone width.
        /// </summary>
        public uint ZoneWidth;

        /// <summary>
        /// The zone length.
        /// </summary>
        public uint ZoneLength;

        /// <summary>
        /// The starting x position.
        /// </summary>
        public uint StartingX;

        /// <summary>
        /// The starting y position.
        /// </summary>
        public uint StartingY;

        /// <summary>
        /// The starting direction.
        /// </summary>
        public char StartingDirection;

        /// <summary>
        /// A character array of commands.
        /// </summary>
        public char[] Commands;
    }
}