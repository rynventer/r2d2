﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Controller.cs" company="">
//   
// </copyright>
// <summary>
//   The controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rover.Core.Process
{
    #region

    using Rover.Core.Datastructures;

    #endregion

    /// <summary>
    /// The controller class is responsible for controlling a spacewagon object from a high level.
    /// </summary>
    public class Controller
    {
        #region Fields

        /// <summary>
        /// The space wagon that the controller is responsible for.
        /// </summary>
        private readonly SpaceWagon spaceWagon;

        /// <summary>
        /// The zone that the spacewagon is in.
        /// </summary>
        private Zone zone;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Controller"/> class.
        /// </summary>
        /// <param name="zone">
        /// The zone that the spacewagon operates in
        /// </param>
        /// <param name="spaceWagon">
        /// The space wagon that will be controlled
        /// </param>
        public Controller(Zone zone, SpaceWagon spaceWagon)
        {
            this.zone = zone;
            this.spaceWagon = spaceWagon;
        }

        #endregion

        #region Public methods and operators

        /// <summary>
        /// This method will return a string that represents the current spacewagon's location and direction
        /// </summary>
        /// <returns>
        /// The <see cref="string"/> return value represents the location and direction of the space wagon in the format "X Y Direction".
        /// </returns>
        public string GetSpaceWagonStatus()
        {
            return this.spaceWagon.StatusToString();
        }

        /// <summary>
        /// This method orders the controller to move the spacewagon according to the passed instruction queue
        /// </summary>
        /// <param name="instructionQueue">
        /// The instruction queue that contains the instructions for the spacewagon.
        /// </param>
        public void Go(InstructionQueue instructionQueue)
        {
            // Initialise with first instruction
            InstructionQueue.Instruction instruction = instructionQueue.DequeueInstruction();

            while (instruction != InstructionQueue.Instruction.Nothing)
            {
                // Evaluate instruction
                switch (instruction)
                {
                    case InstructionQueue.Instruction.MoveForward:
                        {
                            this.spaceWagon.MoveForward();
                            break;
                        }

                    case InstructionQueue.Instruction.TurnLeft:
                        {
                            this.spaceWagon.TurnLeft();
                            break;
                        }

                    case InstructionQueue.Instruction.TurnRight:
                        {
                            this.spaceWagon.TurnRight();
                            break;
                        }
                }

                // Get the next instruction
                instruction = instructionQueue.DequeueInstruction();
            }
        }

        #endregion
    }
}